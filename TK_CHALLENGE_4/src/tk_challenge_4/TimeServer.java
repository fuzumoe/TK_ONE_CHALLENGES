/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tk_challenge_4;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author tk1
 */
public class TimeServer {

    private static int PORT = 27780;
    private ServerSocket serverSocket;
    private int artificialOffset = 1200;
    private ObjectOutputStream pipeToClient;
    private ObjectInputStream pipeFromClient;

    public TimeServer() {
        try {
            serverSocket = new ServerSocket(PORT);
            System.out.println("Server running on port: " + PORT);

            while (true) {

                NTPRequestHandler handler = new NTPRequestHandler(serverSocket.accept());
                Thread t = new Thread(handler);
                t.start();

            }

        } catch (IOException e) {
            e.printStackTrace();
            try {
                serverSocket.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

    }

    private void threadSleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new TimeServer();
    }

    private class NTPRequestHandler implements Runnable {

        private Socket client;

        public NTPRequestHandler(Socket client) {
            this.client = client;
        }

        @Override
        public void run() {
            ///
            try {
                pipeToClient = new ObjectOutputStream(
                        this.client.getOutputStream());
                pipeFromClient = new ObjectInputStream(
                        this.client.getInputStream());
                NTPRequest serNTPRequest = (NTPRequest) pipeFromClient
                        .readObject();
                serNTPRequest.setT2(System.currentTimeMillis() + artificialOffset);
                
		//Random delay between 10ms and 100ms 
                threadSleep((10 + (int) (Math.random() * ((100 - 10) + 1))));

                sendNTPAnswer(serNTPRequest);// reply to Client
             

            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }

        }

        private void sendNTPAnswer(NTPRequest request) throws IOException {
 
            request.setT3(System.currentTimeMillis() + artificialOffset);
            pipeToClient.writeObject(request);
        }

    }

}
