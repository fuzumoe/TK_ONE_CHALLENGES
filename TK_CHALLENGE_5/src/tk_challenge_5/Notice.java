/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tk_challenge_5;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Adam
 */
public class Notice implements Serializable {

    private int NoticeId;
    private String NoticeSender;
    private String NoticeReciever;
    private String NoticeType;
    private Date NoticeDate;
    private int NoticeElements;

    public Notice(int NoticeId, String NoticeSender, String NoticeReciever, String NoticeType, Date NoticeDate, int NoticeElements) {
        this.NoticeId = NoticeId;
        this.NoticeSender = NoticeSender;
        this.NoticeReciever = NoticeReciever;
        this.NoticeType = NoticeType;
        this.NoticeDate = NoticeDate;
        this.NoticeElements = NoticeElements;
    }

    public int getNoticeId() {
        return NoticeId;
    }

    public String getNoticeSender() {
        return NoticeSender;
    }

    public String getNoticeReciever() {
        return NoticeReciever;
    }

    public String getNoticeType() {
        return NoticeType;
    }

    public Date getNoticeDate() {
        return NoticeDate;
    }

    public int getNoticeElements() {
        return NoticeElements;
    }

    @Override
    public String toString() {
        return "Notice{" + "Notice ID = " + NoticeId + ", Notice Sender = " + NoticeSender + ", Notice Server = " + NoticeReciever + ", Notice Type = " + NoticeType + ", Notice Date =" + new SimpleDateFormat("dd/MM/yy HH:mm:ss").format(NoticeDate) + ", MsgContent=" + NoticeElements + '}';
    }

}
