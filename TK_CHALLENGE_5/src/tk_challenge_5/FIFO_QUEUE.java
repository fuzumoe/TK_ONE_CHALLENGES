package tk_challenge_5;

import java.net.DatagramPacket;
import java.util.LinkedList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Adam
 */
public class FIFO_QUEUE  {

    private LinkedList<DatagramPacket> list;

    // Queue constructor
    public FIFO_QUEUE() {
        // Create a new LinkedList.
        list = new LinkedList();
    }
    
    public int QueueSize(){
    return list.size();
    } 
    
    public boolean isEmpty() {
        return (list.isEmpty());
    }

    public void enqueue(DatagramPacket item) {
        // Append the item to the end of our linked list.
        list.add(item);
    }

    public DatagramPacket dequeue() {
        if(!isEmpty()){
            DatagramPacket item = list.get(0);

            list.remove(0);
         return item;
        }
        else{
        return null;
        }    // Return the item
           
    }
}
