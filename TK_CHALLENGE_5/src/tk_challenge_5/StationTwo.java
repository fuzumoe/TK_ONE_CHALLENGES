/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tk_challenge_5;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adam
 */
public class StationTwo {

    public int StationID = 2;
    public int AcountAmount;
    public int NoticeID = 0;
    public int PORT = 2222;
    private FIFO_QUEUE fifoQuee = new FIFO_QUEUE();
    private DatagramSocket dispatchSocket = new DatagramSocket();
    private DatagramSocket dispatchSocketUI = new DatagramSocket();

    private class Lock extends Object {
    }
    private Lock FifoQueueLock = new Lock();
    private Lock StationLock = new Lock();
    //SnapShot Parameters
    private Lock SnapshotStateLock = new Lock();
    private Lock SendingLock = new Lock();//To ensure Sending marker message over each channel before  sending any other message
    private Lock MarkerLock = new Lock();

    private StringBuilder SnapshotStateStringBuilder = new StringBuilder();

    private StringBuilder FromStation1StringBuilder = new StringBuilder();
    private StringBuilder FromStation3StringBuilder = new StringBuilder();

    private boolean MarkerFromStation1IsReceived = false;
    private boolean MarkerFromStation3IsReceived = false;
    private boolean StateIsRecorded = false;
    private boolean IsInitiator = false;

    public StationTwo() throws IOException, SocketException, InterruptedException {
        // System.out.println("Station " + this.StationID + " started");
    }

    /**
     *  //initialize a random amount of money
     *
     * @throws IOException
     * @throws SocketException
     * @throws InterruptedException
     */
//100% correct
    public void CreateRandomAcount() throws IOException, SocketException, InterruptedException {

        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        this.AcountAmount = rand.nextInt((10000 - 1000) + 1) + 1000;
        //  System.out.println(this.AcountAmount);
    }

//100% correct
    private void RunSendingNoticesToFifoQueue() throws SocketException, UnknownHostException, IOException, InterruptedException {

        InetAddress IPAddress = InetAddress.getByName("localhost");

        byte[] incomingData = new byte[1024];

        while (true) {
            //to insure that it will not lead to wrong transfer.
            if (this.AcountAmount == 0) {
                continue;
            }

            Random rand = new Random();

            //int RandomTimeOfDelay = rand.nextInt((500 - 200) + 1) + 200;
  int RandomTimeOfDelay = rand.nextInt((750 - 400) + 1) + 400;           
//  int RandomTimeOfDelay = rand.nextInt((1250 - 500) + 1) + 500;
            //int RandomTimeOfDelay = rand.nextInt((2500 - 1000) + 1) + 1000;
            // int RandomTimeOfDelay = rand.nextInt((5000 - 2000) + 1) + 2000;
            Thread.sleep(RandomTimeOfDelay);

            //Random number that determine to whom this node with send its message.
            int RandomStationToSendTo = rand.nextInt((2 - 1) + 1) + 1;
            //Random number detecet the amount of money to be sent.
            int RandomAmountOfMoney;

            //to insure that it will not lead to a negative account balance.
            while (true) {

                RandomAmountOfMoney = rand.nextInt((50 - 10) + 1) + 10;
                if ((this.AcountAmount - RandomAmountOfMoney) <= 0) {
                    continue;
                } else {
                    break;
                }
            }
            synchronized (StationLock) {
                this.AcountAmount -= RandomAmountOfMoney;
                // System.out.println(this.AcountAmount);
                StationLock.notifyAll();
            }
            NoticeID++;
            Date Noticedate = new Date();
            Notice notice;
            if (RandomStationToSendTo == 1) {
                notice = new Notice(NoticeID, "Station" + StationID, "Station1", "TransferNotice", Noticedate, RandomAmountOfMoney);
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                ObjectOutputStream os = new ObjectOutputStream(outputStream);
                os.writeObject(notice);
                byte[] data = outputStream.toByteArray();
                DatagramPacket sendPacket2 = new DatagramPacket(data, data.length, IPAddress, 1111);

                synchronized (FifoQueueLock) {
                    fifoQuee.enqueue(sendPacket2);
                    FifoQueueLock.notifyAll();
                }

            } else {
                notice = new Notice(NoticeID, "Station" + StationID, "Station3", "TransferNotice", Noticedate, RandomAmountOfMoney);
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                ObjectOutputStream os = new ObjectOutputStream(outputStream);
                os.writeObject(notice);
                byte[] data = outputStream.toByteArray();
                DatagramPacket sendPacket3 = new DatagramPacket(data, data.length, IPAddress, 3333);

                synchronized (FifoQueueLock) {
                    fifoQuee.enqueue(sendPacket3);
                    FifoQueueLock.notifyAll();
                }
            }
        }
    }

//100% correct
    private void GetFromQueueAndSendThrowChannel() throws IOException, InterruptedException, ClassNotFoundException {

        while (true) {

            Random rand = new Random();
            //int RandomTimeOfDelay = rand.nextInt((1000 - 800) + 1) + 800;
            //int RandomTimeOfDelay = rand.nextInt((2500 - 2000) + 1) + 2000;
  int RandomTimeOfDelay = rand.nextInt((1500 - 1000) + 1) + 1000;             
// int RandomTimeOfDelay = rand.nextInt((5000 - 4000) + 1) + 4000;
            // int RandomTimeOfDelay = rand.nextInt((10000 - 8000) + 1) + 8000;
            Thread.sleep(RandomTimeOfDelay);
            synchronized (FifoQueueLock) {
                if (fifoQuee.isEmpty())// {
                {
                    continue;
                }
                // } else {
                synchronized (SendingLock) {
                    DatagramPacket SendingPacket = (DatagramPacket) fifoQuee.dequeue();
                    if (SendingPacket == null) {
                        continue;
                    }

                    dispatchSocket.send(SendingPacket);

                    byte[] data = SendingPacket.getData();
                    ByteArrayInputStream in = new ByteArrayInputStream(data);
                    ObjectInputStream is = new ObjectInputStream(in);
                    Notice notice = (Notice) is.readObject();
                    String NoticeToUI;
                    if (notice.getNoticeType().equals("TransferNotice")) {
                        NoticeToUI = "Transfer: " + notice.getNoticeSender() + " -> " + notice.getNoticeReciever() + "(" + notice.getNoticeElements() + "€)";
                    } else {
                        NoticeToUI = "Send Marker: " + notice.getNoticeSender() + " -> " + notice.getNoticeReciever();
                    }
                    InetAddress IPAddress = InetAddress.getByName("localhost");
                    byte[] sendData = new byte[1024];
                    sendData = NoticeToUI.getBytes();
                    DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, 4444);
                    dispatchSocket.send(sendPacket);
                    SendingLock.notifyAll();
                    FifoQueueLock.notifyAll();
                }
                // }
            }
        }
    }

    private void RunReceivingNotices() throws SocketException, IOException, InterruptedException {

        DatagramSocket serverSocket = new DatagramSocket(PORT);

        byte[] incomingData = new byte[1024];

        while (true) {
            DatagramPacket incomingPacket = new DatagramPacket(incomingData, incomingData.length);

            serverSocket.receive(incomingPacket);

            byte[] data = incomingPacket.getData();

            ByteArrayInputStream in = new ByteArrayInputStream(data);

            ObjectInputStream is = new ObjectInputStream(in);

            try {

                Notice notice = (Notice) is.readObject();
                if (notice.getNoticeType().equals("TransferNotice")) {
                    synchronized (StationLock) {
                        this.AcountAmount += notice.getNoticeElements();
                        StationLock.notifyAll();
                    }
                    //SnapshotAlgorithm
                    //Turning on recording of messages arriving over other incoming channels.
                    if (!IsInitiator) {
                        if (StateIsRecorded && MarkerFromStation1IsReceived) {
                            if (notice.getNoticeSender().equals("Station3")) {
                                synchronized (SnapshotStateLock) {
                                    SnapshotStateStringBuilder.append(notice.getNoticeElements() + ", ");
                                    SnapshotStateLock.notifyAll();
                                }
                            }

                        } else if (StateIsRecorded && MarkerFromStation3IsReceived) {
                            if (notice.getNoticeSender().equals("Station1")) {
                                synchronized (SnapshotStateLock) {
                                    SnapshotStateStringBuilder.append(notice.getNoticeElements() + ", ");
                                    SnapshotStateLock.notifyAll();
                                }
                            }
                        }
                    } else {
                        if (notice.getNoticeSender().equals("Station1")) {
                            FromStation1StringBuilder.append(notice.getNoticeElements() + ", ");
                        } else if (notice.getNoticeSender().equals("Station3")) {
                            FromStation3StringBuilder.append(notice.getNoticeElements() + ", ");
                        }
                    }
                } else {//Marker

                    synchronized (MarkerLock) {
                        if (notice.getNoticeSender().equals("Station1")) {
                            MarkerFromStation1IsReceived = true;
                        } else {
                            MarkerFromStation3IsReceived = true;
                        }
                        MarkerLock.notifyAll();
                    }

                    if (!StateIsRecorded) {
                        synchronized (SnapshotStateLock) {
                            // Recording Acount state now.
                            SnapshotStateStringBuilder.append("Snapshot(Station2:<" + this.AcountAmount + ">, ");
                            //Recording the state of receiving channal as an empty set.
                            SnapshotStateStringBuilder.append("Channel from side of " + notice.getNoticeSender() + "<Empty>, Another Channel<");
                            StateIsRecorded = true;
                            SnapshotStateLock.notifyAll();
                        }

                        //Sending Marker for all Channels.
                        InetAddress IPAddress = InetAddress.getByName("localhost");
                        incomingData = new byte[1024];
                        NoticeID++;
                        Date Noticedate = new Date();
                        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                        ObjectOutputStream os = new ObjectOutputStream(outputStream);

                        Notice notice1 = new Notice(NoticeID, "Station" + StationID, "Station1", "Marker", Noticedate, -1);
                        os.writeObject(notice1);
                        data = outputStream.toByteArray();
                        DatagramPacket sendPacket2 = new DatagramPacket(data, data.length, IPAddress, 1111);
                        synchronized (FifoQueueLock) {
                            fifoQuee.enqueue(sendPacket2);
                            FifoQueueLock.notifyAll();
                        }

                        ByteArrayOutputStream outputStream2 = new ByteArrayOutputStream();
                        ObjectOutputStream os2 = new ObjectOutputStream(outputStream2);
                        Notice notice2 = new Notice(NoticeID, "Station" + StationID, "Station3", "Marker", Noticedate, -1);
                        os2.writeObject(notice2);
                        data = outputStream2.toByteArray();
                        DatagramPacket sendPacket3 = new DatagramPacket(data, data.length, IPAddress, 3333);
                        synchronized (FifoQueueLock) {
                            fifoQuee.enqueue(sendPacket3);
                            FifoQueueLock.notifyAll();
                        }

                    } else {
                        /*
                         if (notice.getNoticeSender().equals("Station1")) {//&& !MarkerFromStation1IsReceived
                         synchronized (MarkerLock) {
                         MarkerFromStation1IsReceived = true;
                         MarkerLock.notifyAll();
                         }

                         } else if (notice.getNoticeSender().equals("Station3")) {// && !MarkerFromStation3IsReceived
                         synchronized (MarkerLock) {
                         MarkerFromStation3IsReceived = true;
                         MarkerLock.notifyAll();
                         }
                         } 
                         */
                        //The end of Snapshot
                        if (MarkerFromStation1IsReceived && MarkerFromStation3IsReceived) {

                            synchronized (MarkerLock) {
                                synchronized (SnapshotStateLock) {
                                    StateIsRecorded = false;
                                    SnapshotStateLock.notifyAll();
                                }
                                MarkerFromStation1IsReceived = false;
                                MarkerFromStation3IsReceived = false;
                                MarkerLock.notifyAll();
                            }
                            InetAddress IPAddress = InetAddress.getByName("localhost");
                            byte[] sendData = new byte[1024];
                            if (!IsInitiator) {
                                synchronized (SnapshotStateLock) {
                                    sendData = SnapshotStateStringBuilder.toString().getBytes();
                                    SnapshotStateStringBuilder.setLength(0);
                                    SnapshotStateLock.notifyAll();
                                }
                            } else {
                                StringBuilder TheFinalSnapShot = new StringBuilder();
                                TheFinalSnapShot.append(SnapshotStateStringBuilder.toString());
                                TheFinalSnapShot.append("From Station1 Channel: " + FromStation1StringBuilder.toString());
                                TheFinalSnapShot.append("From Station3 Channel: " + FromStation3StringBuilder.toString());
                                sendData = TheFinalSnapShot.toString().getBytes();
                                IsInitiator = false;
                            }

                            DatagramPacket sendPacket2 = new DatagramPacket(sendData, sendData.length, IPAddress, 5555);
                            synchronized (SendingLock) {
                                dispatchSocketUI.send(sendPacket2);
                                SendingLock.notifyAll();
                            }
                        }
                    }
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            // System.out.println("SnapShot = = " + SnapshotStateStringBuilder.toString());
        }
    }

//100% correct
    public void RunSnapshotAlgorithm() throws UnknownHostException, IOException {

        synchronized (SendingLock) {//To ensure Sending marker message over each channel before  sending any other message.
            IsInitiator = true;

            synchronized (SnapshotStateLock) {
                SnapshotStateStringBuilder.append("Snapshot(Station2:<" + this.AcountAmount + ">,");
                StateIsRecorded = true;
                SnapshotStateLock.notifyAll();
            }
            InetAddress IPAddress = InetAddress.getByName("localhost");
            NoticeID++;
            Date Noticedate = new Date();
            ByteArrayOutputStream outputStream1 = new ByteArrayOutputStream();
            ByteArrayOutputStream outputStream2 = new ByteArrayOutputStream();

            ObjectOutputStream os1 = new ObjectOutputStream(outputStream1);
            ObjectOutputStream os2 = new ObjectOutputStream(outputStream2);

            Notice notice1 = new Notice(NoticeID, "Station" + StationID, "Station1", "Marker", Noticedate, -1);
            os1.writeObject(notice1);

            Notice notice2 = new Notice(NoticeID, "Station" + StationID, "Station3", "Marker", Noticedate, -1);
            os2.writeObject(notice2);

            byte[] data1 = outputStream1.toByteArray();
            byte[] data2 = outputStream2.toByteArray();

            DatagramPacket sendPacket2 = new DatagramPacket(data1, data1.length, IPAddress, 1111);
            DatagramPacket sendPacket3 = new DatagramPacket(data2, data2.length, IPAddress, 3333);

            synchronized (FifoQueueLock) {
                fifoQuee.enqueue(sendPacket2);
                fifoQuee.enqueue(sendPacket3);
                FifoQueueLock.notifyAll();
            }
            SendingLock.notifyAll();
        }
    }

//100% correct
    public void RunStation() throws IOException, SocketException, InterruptedException {
        this.CreateRandomAcount();

        Thread ReceivingThread = new Thread() {
            public void run() {
                try {
                    RunReceivingNotices();

                } catch (IOException ex) {
                    Logger.getLogger(StationTwo.class
                            .getName()).log(Level.SEVERE, null, ex);
                } catch (InterruptedException ex) {
                    Logger.getLogger(StationTwo.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
        };

        Thread SendingToFifoQueueThread = new Thread() {
            public void run() {
                try {
                    RunSendingNoticesToFifoQueue();

                } catch (IOException ex) {
                    Logger.getLogger(StationTwo.class
                            .getName()).log(Level.SEVERE, null, ex);
                } catch (InterruptedException ex) {
                    Logger.getLogger(StationTwo.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
        };

        Thread SendingToChannelThread = new Thread() {
            public void run() {
                try {
                    GetFromQueueAndSendThrowChannel();

                } catch (IOException ex) {
                    Logger.getLogger(StationTwo.class
                            .getName()).log(Level.SEVERE, null, ex);
                } catch (InterruptedException ex) {
                    Logger.getLogger(StationTwo.class
                            .getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(StationTwo.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
        };

        ReceivingThread.setPriority(Thread.NORM_PRIORITY);
        SendingToFifoQueueThread.setPriority(Thread.NORM_PRIORITY);
        SendingToChannelThread.setPriority(Thread.NORM_PRIORITY);

        ReceivingThread.start();
        SendingToFifoQueueThread.start();
        SendingToChannelThread.start();

    }

}
